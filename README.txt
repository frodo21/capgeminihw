README:

1) how to build application
  - run command in project folder: mvn clean install 
  
2) how to run application
  - build creates .jar file. 
  - locate jar file in command line
  - run: java -jar fm-rest-homework-1.0.jar
  
3) run legacy application on this URL: http://192.168.99.100:10000

4) how test application
	- call following services ("Advanced REST client" extension for browser is recommended):
	
  		- http://localhost:8080/newsystem/ping
  			- should return "pong" String value
  			
  		- http://localhost:8080/newsystem/projects
  			- shows all projects in current database (in JSON Array)
  			- application contains one predefined project "PR33" with one predefined task "uprac kod"
  			
  		- http://localhost:8080/newsystem/import/PRJ1
  			- imports project "PRJ1" from legacy application
  			- returns JSON of imported project
  			
  		- http://localhost:8080/newsystem/projects/PR33/tasks
  			POST Body:{"assignee":"test","status":"OPEN", "name":"new task"}
  			body-content-type: application/json
  			- creates new task under project PR33
  			
  			- you can also use CURL: 
  				curl -X POST --header "Content-Type: application/json" \
  					--data '{"assignee":"test","status":"OPEN", "name":"new task"}'  \
  					http://<server>:<port>/newsystem/projects/PR33/tasks	
  		
-----  			
  			