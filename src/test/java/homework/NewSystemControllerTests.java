/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package homework;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import homework.db.model.Project;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NewSystemControllerTests {

    @Autowired
    private MockMvc mockMvc;
    
    //TODO: Write relevant tests
    
    @Test
    public void newSystemJSONParse() throws Exception {

        String projectStringResponse = "{\"projectId\":\"PRJ1\",\"name\":\"Project1\",\"startDate\":1543061502415,\"tasks\":[{\"assignee\":\"user4\",\"status\":\"OPEN\",\"name\":\"Task1\",\"description\":\"Description1\",\"creationDate\":1543061502415},{\"assignee\":\"user2\",\"status\":\"OPEN\",\"name\":\"Task2\",\"description\":\"Description2\",\"creationDate\":1543061502415},{\"assignee\":\"user3\",\"status\":\"IN_PROGRESS\",\"name\":\"Task3\",\"description\":\"Description3\",\"creationDate\":1543061502415},{\"assignee\":\"user4\",\"status\":\"OPEN\",\"name\":\"Task4\",\"description\":\"Description4\",\"creationDate\":1543061502415}]}";
    	
    	ObjectMapper objectMapper = new ObjectMapper();

    	Project project = null;
 		
    	try {
 			project = objectMapper.readValue(projectStringResponse, Project.class);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}
    	
 		System.out.println("projectId="+project.getProjectId());
 		
 		 this.mockMvc.perform(get("/newsystem/ping")).andDo(print()).andExpect(status().isOk());
 		
    }

}
