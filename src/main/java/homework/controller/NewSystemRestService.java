package homework.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import homework.db.dao.ProjectDAO;
import homework.db.dao.TaskDAO;
import homework.db.model.Project;
import homework.db.model.Task;

@RestController
@RequestMapping(path = "/newsystem")
public class NewSystemRestService {

	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private TaskDAO taskDAO;

	@Value("${legacy.baseurl}") // TODO: load from properties file
	private static String legacyBaseurl = "http://192.168.99.100:10000";
    private static final String legacyUri = legacyBaseurl + "/legacy/resources/project/%s";

    
    
    @RequestMapping("/ping")
    public String ping() {
        return "pong"; 
    }
    
    @RequestMapping(value="/import/{projectId}", method = RequestMethod.GET)
    public Project makeImport(@PathVariable("projectId") String projectId) {
    	
    	RestTemplate restTemplate = new RestTemplate();
    	String uri =  String.format(legacyUri, projectId);
    	String projectStringResponse = restTemplate.getForObject(uri, String.class);
    			
    	ObjectMapper objectMapper = new ObjectMapper();
    	Project project = null;
		try {
			project = objectMapper.readValue(projectStringResponse, Project.class);
			projectDAO.save(project);
			Set<Task> tasks = project.getTasks();
			
			for(Task t : tasks) {
				t.setProject(project);
				taskDAO.save(t);
			}
			
		} catch (Exception e) {
			e.printStackTrace(); // TODO: handle exceptions
		}
    	
    	return project;
    }
    
    
    @RequestMapping("/projects")
    public List<Project> printProjects() {
    	
    	Iterable<Project> all = projectDAO.findAll();
    	List<Project> prs = new ArrayList<>();
        all.forEach(p -> prs.add(p));
    	
        return prs;
    }
    
    @RequestMapping(value="/projects/{projectId}/tasks", method = RequestMethod.POST)
    public Task createTask(@PathVariable("projectId") String projectId, @RequestBody Task task) {
    	
    	Project project = projectDAO.findByProjectId(projectId);
    	task.setCreationDate(new Date());
    	task.setProject(project);
    	
    	
    	taskDAO.save(task);
    	
    	return task;
    }
}
