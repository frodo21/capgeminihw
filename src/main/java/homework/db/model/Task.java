package homework.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TASK")
public class Task {

	public Task() {
	}

	public Task(String assignee, String status, String name, String description, Date creationDate, Project project) {
		this.assignee = assignee;
		this.status = status;
		this.name = name;
		this.description = description;
		if (creationDate == null) {
			this.creationDate = new Date();
		} else {
			this.creationDate = creationDate;
		}
		this.project = project;
	}

	@Column(name = "assignee", nullable = false)
	private String assignee;

	@Column(name = "status", nullable = false)
	private String status;

	@Column(name = "description", nullable = true)
	private String description;

	@Id
	@Column(name = "name", length = 64, nullable = false)
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "creationDate", nullable = false)
	private Date creationDate;

	@ManyToOne
	@JoinColumn(name = "project")
	private Project project;

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProject() {
		return project.getProjectId();
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		if (creationDate == null) {
			this.creationDate = new Date();
		} else {
			this.creationDate = creationDate;
		}
	}

}