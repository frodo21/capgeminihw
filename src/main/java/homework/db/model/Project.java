package homework.db.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PROJECT")
public class Project {

	public Project() {
	}

	public Project(String projectId, String name, Date startDate, Set<Task> tasks) {
		this.projectId = projectId;
		this.name = name;
		this.startDate = startDate;
		this.tasks = tasks;
	}

	@Id
	@Column(name = "projectId", nullable = false)
	private String projectId;

	@Column(name = "name", length = 64, nullable = false)
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "startDate", nullable = false)
	private Date startDate;

	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private Set<Task> tasks;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

}