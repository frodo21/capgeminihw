package homework.db.dao;
 
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import homework.db.model.Task;
 
@Repository
public interface TaskDAO extends CrudRepository<Task, Long> {
 
    public List<Task> findByNameLike(String name);
 
}