package homework.db.dao;
 
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import homework.db.model.Project;
 
@Repository
public interface ProjectDAO extends CrudRepository<Project, Long> {
 
    public List<Project> findByNameLike(String name);
 
    public List<Project> findByStartDateGreaterThan(Date date);
    
    public Project findByProjectId(String projectId);
 
}