package homework;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import homework.db.dao.ProjectDAO;
import homework.db.dao.TaskDAO;
import homework.db.model.Project;
import homework.db.model.Task;
 
@Component
public class DataInit implements ApplicationRunner {
 
	private ProjectDAO projectDAO;
	private TaskDAO taskDAO;
 
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
 
    @Autowired
    public DataInit(ProjectDAO projectDAO, TaskDAO taskDAO) {
        this.projectDAO = projectDAO;
        this.taskDAO = taskDAO;
    }
 
    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = projectDAO.count();
 
        if (count == 0) {
        	Project p1 = new Project();
            p1.setProjectId("PR33");
            p1.setStartDate(df.parse("1980-12-20"));
            p1.setName("generovany projekt 33");
            
//            Project p2 = new Project();
//            p2.setProjectId("PR22");
//            p2.setStartDate(df.parse("1989-12-20"));
//            p2.setName("generovany projekt 22");
            
 
            projectDAO.save(p1);
//            projectDAO.save(p2);
            
            Task t1 = new Task("jozo1", "STATUS2", "uprac kod", "uprac vsetky trashe v kode", new Date(), p1);
//            Task t2 = new Task("jozo2", "STATUS2", "uprac kod", "uprac vsetky trashe v kode", new Date(), p1);
//            Task t3 = new Task("jozo3", "STATUS2", "uprac kod", "uprac vsetky trashe v kode", new Date(), p1);
//            Task t4 = new Task("jozo4", "STATUS2", "uprac kod", "uprac vsetky trashe v kode", new Date(), p1);
            taskDAO.save(t1);
//            taskDAO.save(t2);
//            taskDAO.save(t3);
//            taskDAO.save(t4);
        }
 
    }
     
}